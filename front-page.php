<?php 
    $display = get_field('visible') ? 'display: none;': ''; 
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <!-- My Style -->
    <?php wp_head(); ?>
    <!-- / My Style -->

    <title>Comparehuilv.com</title>
  </head>
  <body>
    <?php 
    if ( have_posts() ) {
        while ( have_posts() ) {
            the_post();
    ?>
    <section class="head">
        <div class="container ">
            <div class="row align-items-center">
                
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-left">
                    <div>
                        <h1>Comparehuilv.cn</h1>
                    </div>
                </div>
                
                <div class="head_title_m col-10 col-sm-10 col-md-11 col-lg-11 col-xl-11 d-lg-none d-xl-none">
                    <h2><?php the_field( "broker_review_2018" );?></h2>  
                </div>
                
                <div class="head_content_m_1 col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 text-justify d-lg-none d-xl-none">
                    <p> 
                        <?php the_field('do_you_want_to_start_trading'); ?>
                    </p>
                </div>
                
                <div class="head_content_m_2 col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 text-justify d-lg-none d-xl-none">
                    <p>
                        <?php the_field('our_broker_review_2018_shows'); ?>
                    </p>
                </div>
            </div>
        </div>
    </section>  
    <section class="s1">
        <div class="container">

            <div class="row align-items-center d-none d-lg-flex d-xl-flex">
                <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">
                    <img src="<?php echo(get_template_directory_uri().'/img/1.png');?>" alt="">
                </div>
                <div class="col-10 col-sm-10 col-md-11 col-lg-11 col-xl-11">
                    <h2><?php the_field( "broker_review_2018" );?></h2>  
                </div>
            </div>

            <div class="row align-items-top d-none d-lg-flex d-xl-flex">
                <div class=" col-sm-12 col-md-12 col-lg-6 col-xl-6 text-justify">
                    <p> 
                        <?php the_field('do_you_want_to_start_trading'); ?>
                    </p>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 text-justify">
                    <p>
                        <?php the_field('our_broker_review_2018_shows'); ?>
                    </p>
                </div>
            </div>

        </div>
    </section>
    <!--==== Table PC view ====-->
    <section class="s2 d-none d-lg-block d-xl-block">
        <div class="container div-table">
            <div class="row">
                <div class="col-12">
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col"></th>
                        <th scope="col"><?php the_field('instruments'); ?></th>
                        <th scope="col"><?php the_field('regulation'); ?></th>
                        <th scope="col"><?php the_field('benefits'); ?></th>
                        <th scope="col"><?php the_field('start_trading'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="tr_1" style="<?php echo( $display ); ?>">
                            <td>
                                <div class="left"><p>1</p></div>
                                <div class=" always"><p><i></i><?php the_field('rating'); ?> 10</p></div>
                                <div class="logo">
                                    <img src="<?php echo(get_template_directory_uri().'/img/logo_1_-min.png');?>" alt="">
                                </div>
                            </td>
                                
                            <td>
                                <?php the_field('cfds_on_fx');?>, <br>
                                <?php the_field('shares');?>, 
                                <?php the_field('indices');?>,<br> 
                                <?php the_field('futures');?>, 
                                <?php the_field('energies');?>,<br> 
                                <?php the_field('metals');?>
                            </td>
                            
                            <td> <?php the_field('fca');?>,  <?php the_field('cysec');?>, <br>  <?php the_field('fsb');?>,  <?php the_field('dfsa');?>, <br>  <?php the_field('scb');?></td>
                            
                            <td>
                                <div class="row benefits-td">
                                    <div class="col-2 text-right"><div class="ok_hiden"></div></div>
                                    <div class="col-10"><?php the_field('regulated'); ?></div>

                                    <div class="col-2"><div class="ok_hiden"></div></div>
                                    <div class="col-10"><?php the_field('280+_instruments'); ?></div>

                                    <div class="col-2"><div class="ok_hiden"></div></div>
                                    <div class="col-10">
                                        <?php the_field('tight_spread'); ?> 
                                        <?php the_field('mt4_mt5_ctrader_platforms_available'); ?>
                                    </div>

                                    <div class="col-2"><div class="ok_hiden"></div></div>
                                    <div class="col-10"><?php the_field('negative_balance_protection*'); ?></div>
                                    
                                    <div class="col-2"><div class="ok_hiden"></div></div>
                                    <div class="col-10"><?php the_field('24/5_customer_support'); ?></div>
                                </div>
                            </td>
                            <td class="btn-start-trading hidden-lg-up">
                                <a href="https://www.fxpro.cn/" class="btn btn-success my-btn"><?php the_field('start_trading_fxpro'); ?></a>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <div class="left"><p>2</p></div>
                                <div class="right"><p><i></i> <?php the_field('rating'); ?> 9.1</p></div>
                                <div class="logo">
                                    <img src="<?php echo(get_template_directory_uri().'/img/logo_2_-min.png');?>" alt="">
                                </div>
                            </td>

                            <td>
                                <?php the_field('stocks'); ?>, 
                                <?php the_field('funds'); ?>,  
                                <br> <?php the_field('bonds_&_futures'); ?> 
                            </td>

                            <td><?php the_field('cbrc_china_banking_regulatory_commision');?></td>
                          
                            <td>
                                <div class="row benefits-td">
                                        <div class="col-2 text-right"><div class="ok"></div></div>
                                        <div class="col-10"><?php the_field('regulated'); ?></div>

                                        <div class="col-2"><div class="ok"></div></div>
                                        <div class="col-10"><?php the_field('customer_support'); ?></div>

                                        <div class="col-2"><div class="ok"></div></div>
                                        <div class="col-10"><?php the_field('banking_services_available'); ?></div>
                                    </div>
                            </td>

                            <td><p>Offline registration</p><a href="http://www.htsc.com.cn/htzq/index/index.jsp"><?php the_field('learn_more'); ?></a></td>
                        </tr>

                        <tr>
                            <td>
                                <div class="left"><p>3</p></div>
                                <div class="right"><p><i></i> <?php the_field('rating'); ?> 7.0</p></div>
                                <div class="logo">
                                    <img src="<?php echo(get_template_directory_uri().'/img/logo_3_-min.png');?>" alt="">
                                </div>
                            </td>
                           
                            <td>
                                <?php the_field('securities'); ?>, 
                                <br> <?php the_field('stocks'); ?>,
                                <?php the_field('futures'); ?>, 
                            </td>

                           <td><?php the_field('cbrc_china_banking_regulatory_commision');?></td>
                            <td>
                                <div class="row benefits-td">
                                    <div class="col-2 text-right"><div class="ok"></div></div>
                                    <div class="col-10"><?php the_field('regulated'); ?></div>

                                    <div class="col-2"><div class="ok"></div></div>
                                    <div class="col-10"><?php the_field('customer_support'); ?></div>

                                    <div class="col-2"><div class="ok"></div></div>
                                    <div class="col-10"><?php the_field('banking_services_available'); ?></div>
                                </div>
                            </td>
                            <td><p>Offline registration</p><a href="http://www.citics.com.hk/Default.aspx"><?php the_field('learn_more'); ?></td>
                        </tr>

                        <tr>
                            <td>
                                <div class="left"><p>4</p></div>
                                <div class="right"><p><i></i> <?php the_field('rating'); ?> 6.5</p></div>
                                <div class="logo">
                                    <img src="<?php echo(get_template_directory_uri().'/img/logo_4_-min.png');?>" alt="">
                                </div>
                            </td>
                                
                            <td>
                                <?php the_field('securities'); ?>,
                                <?php the_field('futures'); ?>
                            </td>
                           
                            <td>
                                <?php the_field('cbrc_china_banking_regulatory_commision');?>
                            </td>
                          
                            <td>
                                <div class="row benefits-td">
                                    <div class="col-2 text-right"><div class="ok"></div></div>
                                    <div class="col-10"><?php the_field('regulated'); ?></div>

                                    <div class="col-2"><div class="ok"></div></div>
                                    <div class="col-10"><?php the_field('customer_support'); ?></div>

                                    <div class="col-2"><div class="ok"></div></div>
                                    <div class="col-10"><?php the_field('banking_services_available'); ?></div>
                                </div>
                            </td>
                            <td>
                                <p>Offline registration</p><a href="http://www.haitongib.com/"><?php the_field('learn_more'); ?></a>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <div class="left"><p>5</p></div>
                                <div class="right"><p><i></i> <?php the_field('rating'); ?> 6.5</p></div>
                                <div class="logo">
                                    <img src="<?php echo(get_template_directory_uri().'/img/logo_5_-min.png');?>" alt="">
                                </div>
                            </td>
                                
                            <td>
                                <?php the_field('securities'); ?>
                            </td>
                            
                            <td><?php the_field('cbrc_china_banking_regulatory_commision');?></td>
                            
                            <td>
                                <div class="row benefits-td">
                                    <div class="col-2 text-right"><div class="ok"></div></div>
                                    <div class="col-10"><?php the_field('regulated'); ?></div>

                                    <div class="col-2"><div class="ok"></div></div>
                                    <div class="col-10"><?php the_field('customer_support'); ?></div>

                                    <div class="col-2"><div class="ok"></div></div>
                                    <div class="col-10"><?php the_field('banking_services_available'); ?></div>
                                </div>
                            </td>

                            <td>
                                <p>Offline registration</p><a href="http://www.gjzq.com.cn/"><?php the_field('learn_more'); ?></a>
                            </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--==== /Table PC view ====-->

    <!--==== Table Mobile view ====-->
    <section class="s2m d-lg-none d-xl-none">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active" style="<?php echo( $display ); ?>">
                    <div class="container tr tr_1">
                        <div class="row text-left">
                            <div class="col-6">
                                <div class="left"><p>1</p></div>
                                <div class=" always"><p><i></i><?php the_field('rating'); ?> 10</p></div>
                            </div>
                            <div class="col-6">
                                <img src="<?php echo(get_template_directory_uri().'/img/logo_1_-min.png');?>" alt="">
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">INSTRUMENTS</p>      
                            </div>
                            <div class="col-12">
                                <?php the_field('cfds_on_fx');?>, 
                                <?php the_field('shares');?>, 
                                <?php the_field('indices');?>,
                                <?php the_field('futures');?>, 
                                <?php the_field('energies');?>,
                                <?php the_field('metals');?>
                            </div>
                        </div>   
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">REGULATION</p>      
                            </div>
                            <div class="col-12">
                                <p>
                                    <?php the_field('fca');?>,  <?php the_field('cysec');?>, 
                                    <?php the_field('fsb');?>,  <?php the_field('dfsa');?>, 
                                    <?php the_field('scb');?>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">BENEFITS</p>      
                            </div>
                            <div class="col-2  text-left">
                                <div class="ok_hiden"></div>
                            </div>
                            <div class="col-10">
                                <?php the_field('regulated'); ?>
                            </div>

                            <div class="col-2">
                                <div class="ok_hiden"></div>
                            </div>
                            <div class="col-10">
                                <?php the_field('customer_support'); ?>
                            </div>

                            <div class="col-2">
                                <div class="ok_hiden"></div>
                            </div>
                            <div class="col-10">
                                <?php the_field('banking_services_available'); ?>
                            </div>

                            <div class="col-2">
                                <div class="ok_hiden"></div>
                            </div>
                            <div class="col-10">
                                <?php the_field('negative_balance_protection*'); ?>
                            </div>
                            
                            <div class="col-2">
                                <div class="ok_hiden"></div>
                            </div>
                            <div class="col-10">
                                <?php the_field('24/5_customer_support'); ?>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col">
                                <a href='https://www.fxpro.cn/' type="button" class="btn btn-success my-btn-mobile"><p id="my-btn-m"><?php the_field('start_trading_fxpro'); ?></p></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="container tr tr_2">

                        <div class="row text-left">
                            <div class="col-6">
                                <div class="left"><p>2</p></div>
                                <div class="right"><p><i></i><?php the_field('rating'); ?> 9.1</p></div>
                            </div> 
                            <div class="col-6">
                                <img src="<?php echo(get_template_directory_uri().'/img/logo_2_-min.png');?>" alt="">
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">INSTRUMENTS</p>      
                            </div>
                            <div class="col-12">
                                <?php the_field('stocks'); ?>, 
                                <?php the_field('funds'); ?>,  
                                <?php the_field('bonds_&_futures'); ?> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">REGULATION</p>      
                            </div>
                            <div class="col-12">
                                <p>
                                    <?php the_field('cbrc_china_banking_regulatory_commision');?>
                                </p>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">BENEFITS</p>      
                            </div>
                            <div class="col-2">
                                <div class="ok"></div>
                            </div>
                            <div class="col-10">
                                <?php the_field('regulated'); ?>
                                </div>

                            <div class="col-2">
                                <div class="ok"></div>
                            </div>
                            <div class="col-10">
                                <?php the_field('customer_support'); ?>
                            </div>

                            <div class="col-2">
                                <div class="ok"></div>
                            </div>
                            <div class="col-10">
                                <?php the_field('banking_services_available'); ?>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col">
                                <a href="http://www.htsc.com.cn/htzq/index/index.jsp"><?php the_field('learn_more'); ?></a>
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="carousel-item">
                    <div class="container tr tr_3">
                        <div class="row text-left">
                            <div class="col-6">
                                <div class="left"><p>3</p></div>
                                <div class="right"><p><i></i><?php the_field('rating'); ?> 9.1</p></div>
                            </div> 
                            <div class="col-6">
                                <img src="<?php echo(get_template_directory_uri().'/img/logo_3_-min.png');?>" alt="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">INSTRUMENTS</p>      
                            </div>
                            <div class="col-12">
                                <?php the_field('securities'); ?>, 
                                <?php the_field('stocks'); ?>,
                                <?php the_field('futures'); ?>,    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">REGULATION</p>      
                            </div>
                            <div class="col-12">
                                <p>
                                    <?php the_field('cbrc_china_banking_regulatory_commision');?>   
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">BENEFITS</p>      
                            </div>
                            <div class="col-2 text-right"><div class="ok"></div></div>
                            <div class="col-10"><?php the_field('regulated'); ?></div>

                            <div class="col-2"><div class="ok"></div></div>
                            <div class="col-10"><?php the_field('customer_support'); ?></div>

                            <div class="col-2"><div class="ok"></div></div>
                            <div class="col-10"><?php the_field('banking_services_available'); ?></div>
                        </div>
                        <div class="row text-center">
                            <div class="col">
                                <a href="http://www.citics.com.hk/Default.aspx"><?php the_field('learn_more'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="container tr tr_4">
                        <div class="row text-left">
                            <div class="col-12">
                                <div class="left"><p>4</p></div>
                                <div class="right"><p><i></i><?php the_field('rating'); ?> 9.1</p></div>
                            </div>
                            <div class="col-12">
                                <img src="<?php echo(get_template_directory_uri().'/img/logo_4_-min.png');?>" alt="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">INSTRUMENTS</p>      
                            </div>
                            <div class="col">
                                <?php the_field('securities'); ?>,
                                <?php the_field('futures'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">REGULATION</p>      
                            </div>
                            <div class="col-12">
                                <p>
                                    <?php the_field('cbrc_china_banking_regulatory_commision');?>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">BENEFITS</p>      
                            </div>
                            <div class="col-2 text-right">
                            <div class="ok"></div></div>
                            <div class="col-10"><?php the_field('regulated'); ?></div>

                            <div class="col-2"><div class="ok"></div></div>
                            <div class="col-10"><?php the_field('customer_support'); ?></div>

                            <div class="col-2"><div class="ok"></div></div>
                            <div class="col-10"><?php the_field('banking_services_available'); ?></div>
                        </div>
                        <div class="row text-center">
                            <div class="col">
                                <a href="http://www.haitongib.com/"><?php the_field('learn_more'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="container tr tr_5">
                        <div class="row text-left">
                            <div class="col-6">
                                <div class="left"><p>5</p></div>
                                <div class="right"><p><i></i><?php the_field('rating'); ?> 9.1</p></div>
                            </div>
                            <div class="col">
                                <img src="<?php echo(get_template_directory_uri().'/img/logo_5_-min.png');?>" alt="">
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">INSTRUMENTS</p>      
                            </div>
                            <div class="col-12">
                                <?php the_field('securities'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">REGULATION</p>      
                            </div>
                            <div class="col-12">
                                <p>
                                    <?php the_field('cbrc_china_banking_regulatory_commision');?>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="title_table_mobile_section">BENEFITS</p>      
                            </div>
                            <div class="col-2 text-right">
                            <div class="ok"></div></div>
                            <div class="col-10"><?php the_field('regulated'); ?></div>

                            <div class="col-2"><div class="ok"></div></div>
                            <div class="col-10"><?php the_field('customer_support'); ?></div>

                            <div class="col-2"><div class="ok"></div></div>
                            <div class="col-10"><?php the_field('banking_services_available'); ?></div>
                        </div>
                        <div class="row text-center">
                            <div class="col">
                                <a href="http://www.gjzq.com.cn/"><?php the_field('learn_more'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div> 




    </section>
    <!--==== /Table Mobile view ====-->
    
    <section class="s3">
        <div class="container">
            <div class="row title">
                
                <div class="col-12  col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <h2><?php the_field('about_broker_review_2018');?></h2>
                </div>
                
                <div class="col-12  col-sm-12 col-md-12 col-lg-12 col-xl-12 text-justify">
                    <p>
                        <?php the_field('for_our_broker_review_2018');?>	
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="row align-items-center">
                        <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">
                            <img src="<?php echo(get_template_directory_uri().'/img/2.jpg');?>" alt="">
                        </div>
                        <div class="col-10 col-sm-11 col-md-11 col-lg-11 col-xl-11">
                            <h3><?php the_field('regulation');?></h3>
                        </div>
                    </div>
                    <div class="row description">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-justify">
                            <p> 
                            <?php the_field('as_any_trader_will_tell_you');?>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="row align-items-center">
                        <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">
                            <img src="<?php echo(get_template_directory_uri().'/img/2.jpg');?>" alt="">
                        </div>
                        <div class="col-10 col-sm-11 col-md-11 col-lg-11 col-xl-11 ">
                            <h3> <?php the_field('trading_conditions');?></h3>
                        </div>
                    </div>
                    <div class="row description">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-justify">
                            <p> 
                                <?php the_field('how_good_are_the_trading_conditions');?>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="row align-items-center">
                        <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">
                            <img src="<?php echo(get_template_directory_uri().'/img/2.jpg');?>" alt="">
                        </div>
                        <div class="col-10 col-sm-11 col-md-11 col-lg-11 col-xl-11">
                            <h3><?php the_field('trading_products');?></h3>
                        </div>
                    </div>
                    <div class="row description">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-justify">
                            <p> <?php the_field('each_trader_has_different_requirements');?>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="row align-items-center">
                        <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">
                            <img src="<?php echo(get_template_directory_uri().'/img/2.jpg');?>" alt="">
                        </div>
                        <div class="col-10 col-sm-11 col-md-11 col-lg-11 col-xl-11">
                            <h3><?php the_field('registration');?></h3>
                        </div>
                    </div>
                    <div class="row description">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-justify">
                            <p> <?php the_field('how_effective_is_the_registration_process');?>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <div class="row align-items-center">
                        <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">
                            <img src="<?php echo(get_template_directory_uri().'/img/2.jpg');?>" alt="">
                        </div>
                        <div class="col-10 col-sm-11 col-md-11 col-lg-11 col-xl-11">
                            <h3><?php the_field('customer_support');?></h3>
                        </div>
                    </div>
                    <div class="row description">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-justify">
                            <p> <?php the_field('customer_support_is_very_important_for_traders');?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <section class="footer">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                    <h4><?php the_field('we_hope_that_you_benefit_from_our_examinatio');?></h4>
                </div>
            </div>
        </div>
    </section>
    <section class="our_team d-none d-lg-block d-xl-block">
        <div class="container">
            <div class="row text-center">
                <div class="col">
                    <h2><?php the_field('our_team'); ?></h2>
                </div>
            </div>
            <div class="row team text-center">
            <div class="col">
                    <div class="icn_team">
                        <img src="<?php echo(get_template_directory_uri()."/img/new/icon_p_1.png"); ?>" alt="">
                    </div>
                    <div class="about_team">
                      <p><b><?php the_field('li_meili'); ?></b></p>
                       <p><?php the_field('partnerships_executive'); ?></p>
                    </div>
                </div>

                <div class="col">
                    <div class="icn_team">
                        <img src="<?php echo(get_template_directory_uri()."/img/new/icon_p_2.png"); ?>" alt="">
                    </div>
                    <div class="about_team">
                        <p><b><?php the_field('zhang_guangfu'); ?></b></p>
                        <p><?php the_field('founder_professional_trader'); ?></p>
                    </div>
                </div>

                <div class="col">
                    <div class="icn_team">
                        <img src="<?php echo(get_template_directory_uri()."/img/new/icon_p_3.png"); ?>" alt="">
                    </div>
                    <div class="about_team">
                        <p><b><?php the_field('wang_keai'); ?></b></p>
                        <p><?php the_field('professional_trader'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="our_team d-lg-none d-xl-none">
        <div class="container">
            <div class="row text-center">
                <div class="col">
                    <h2><?php the_field('our_team'); ?></h2>
                </div>
            </div>
            <div class="row team text-center">
                <!-- ===================== -->
                <div id="carouselExampleControls2" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner c-inner">
                        <div class="carousel-item active">
                            <div class="col">
                                <div class="icn_team">
                                    <img src="<?php echo(get_template_directory_uri()."/img/new/icon_p_1.png"); ?>" alt="">
                                </div>
                                <div class="about_team">
                                <p><b><?php the_field('li_meili'); ?></b></p>
                                <p><?php the_field('partnerships_executive'); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col">
                                <div class="icn_team">
                                    <img src="<?php echo(get_template_directory_uri()."/img/new/icon_p_2.png"); ?>" alt="">
                                </div>
                                <div class="about_team">
                                    <p><b><?php the_field('zhang_guangfu'); ?></b></p>
                                    <p><?php the_field('founder_professional_trader'); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col">
                                <div class="icn_team">
                                    <img src="<?php echo(get_template_directory_uri()."/img/new/icon_p_3.png"); ?>" alt="">
                                </div>
                                <div class="about_team">
                                    <p><b><?php the_field('wang_keai'); ?></b></p>
                                    <p><?php the_field('professional_trader'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev prev " href="#carouselExampleControls2" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next next " href="#carouselExampleControls2" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>   
                <!-- ===================== -->

            </div>
        </div>
    </section>
    <section class="contact_us">
        <div class="container">
            <div class="row text-center">
                <div class="col">
                    <h2><? the_field('contact_us')?></h2>
                </div>
            </div>
            <div class="row text-center">
                <div class="col">
                    <?php echo (do_shortcode( '[contact-form-7 id="49" title="Contact form 1"]' )) ?>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <address>
                    地址：北京朝阳区, 新源南路 10 号启皓东塔 8 层 202, 北京，100027，中国
                    </address>
                </div>
                <div class="col-12 text-center">
                    <address>
                    电话：13817215243
                    </address>
                </div>
                <div class="col-12 text-center">
                    <address>
                    交易教室有限公司
                    </address>
                </div>
            </div>
        </div>
    </section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <?php wp_footer(); ?>
        <?php
    } // end while
    } // end if
    ?>
</body>
</html>