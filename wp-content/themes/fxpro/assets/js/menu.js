( function( $ ){
    $( document ).ready( function(){
        /**
         * Обработка нажатия кнопки переключения языков в шапке
         */
        $( '.language' ).on( 'click', function(){
            $('.lang-wrapper').slideDown();
        } );

        /**
         * Обработка нажатия кнопки скрытия списка языков в шапке
         */
        $( '.language-close' ).on( 'click', function(){
            $('.lang-wrapper').slideUp();
        } );
    } );
    
} )( jQuery );