<?php

    add_action( 'wp_enqueue_scripts', function(){
        wp_enqueue_style( 'bootstrap_4', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' );
        wp_enqueue_style( 'fa', 'https://use.fontawesome.com/releases/v5.0.9/css/all.css' );
        wp_enqueue_style( 'style', get_stylesheet_uri() );
    } );

?>