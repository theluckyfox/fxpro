<div class="row">
	<div class="col-xl-8">
		<div class="row">
			<div class="col-12 col-xl-12">
				<h2 class="marketing-title">Logos</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-xl-12 logo_img_block_1">
				<div class="row">
					<div class="col-12 col-xl-12">
						<h4 class='sub-title'>Official FxPro logo</h4>
					</div>
					<div class="col-12 col-xl-12">
						<img src="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_general_logo.jpg' ?>" alt="" style="width: 300px; height: 300px;">
					</div>
					<div class="col-12 col-xl-12">
						<div class="row expansion">
							<div class="col-2 col-md-2 col-xl-1"><a class='button_gray' href="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_general_logo.jpg' ?>">.png</a></div>
							<div class="col-2 col-md-2 col-xl-1"><a class='button_gray' href="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_general_logo.jpg' ?>">.eps</a></div>
							<div class="col-2 col-md-2 col-xl-1"><a class='button_gray' href="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_general_logo.jpg' ?>">.jpg</a></div>
							<div class="col-2 col-md-2 col-xl-1"><a class='button_gray' href="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_general_logo.jpg' ?>">.ai</a></div>
						</div>  
					</div>
				</div>

			</div>
			<div class="col-12 col-xl-6 logo_img_block_2">
				<div class="row">
					<div class="col-12 col-xl-12">
						<h4 class='sub-title'>FxPro product`s logos</h4>
					</div>
				</div>
					<!-- 1 -->
				<div class="row logo_block_1">
					<div class="col-6 col-xl-6">
						<img src="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_quant.png' ?>" alt="" style="width: 135px; height: 67px;">
					</div>
					<div class="col-6 col-xl-6">
						<div class="row">
							<div class="col-12 col-xl-auto">
								<h4 class='col-title'>FxPro Quant</h4>
							</div>
							<div class="col-6 col-xl-12"><a class='button_gray' href="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_quant.png' ?>">.png</a></div>
							<div class="col-6 col-xl-12"><a class='button_gray' href="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_quant.eps' ?>">.eps</a></div>
						</div>
					</div>
				</div>
					
					<!-- 2 -->
				<div class="row logo_block_2">	
					<div class="col-6 col-xl-6">
						<img src="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_vps.png' ?>" alt="" style="width: 135px; height: 102px;">
					</div>
					<div class="col-6 col-xl-6">
						<div class="row">
							<div class="col-12 col-xl-auto">
								<h4 class='col-title'>FxPro VPS</h4>
							</div>
							<div class="col-6 col-xl-12"><a class='button_gray' href="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_vps.png' ?>">.png</a></div>
							<div class="col-6 col-xl-12"><a class='button_gray' href="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_vps.eps' ?>">.eps</a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-xl-6 logo_img_block_3">
					<!-- 1 -->
				<div class="row logo_block_3">
					<div class="col-6 col-xl-6">
					<img src="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_dashboard.png' ?>" alt="" style="width: 135px; height: 67px;">
						<img src="" alt="">
					</div>
					<div class="col-6 col-xl-6">
						<div class="row">
							<div class="col-12 col-xl-auto">
								<h4 class='col-title'>FxPro Dashboard</h4>
							</div>
							<div class="col-6 col-xl-12"><a class='button_gray' href="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_dashboards.png' ?>">.png</a></div>
							<div class="col-6 col-xl-12"><a class='button_gray' href="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_dashboards.eps' ?>">.eps</a></div>
						</div>
					</div>
				</div>
					<!-- 2 -->
				<div class="row logo_block_4">	
					<div class="col-6 col-xl-6">
						<img src="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_webinars.png' ?>" alt="" style="width: 135px; height: 67px;">
						<img src="" alt="">
					</div>
					<div class="col-6 col-xl-6">
						<div class="row">
							<div class="col-12 col-xl-auto">
								<h4 class='col-title'>FxPro Webinars</h4>
							</div>
							<div class="col-6 col-xl-12"><a class='button_gray' href="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_webinars.png' ?>">.png</a></div>
							<div class="col-6 col-xl-12"><a class='button_gray' href="<?= get_template_directory_uri() . '/assets/img/logo/fxpro_webinars.eps' ?>">.eps</a></div>
						</div>
					</div>
				</div>	

			</div>
		</div>  
	</div>


	<div class="ib-link-builder col-xl-4">
		<iframe src="https://widgets-m.fxpro.com/en/statistics/ib-link" style="width: 100%; height: 400px; border: none;"></iframe>
	</div>
</div>