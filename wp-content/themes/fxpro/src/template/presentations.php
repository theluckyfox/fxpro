<div class="row presentations-block">
	<div class="col-xl-8">
		<div class="row">
			<div class="col-12">
				<h2 class="marketing-title">Presentations</h2>
			</div>
		</div>		
	<?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
		<div class="row">
			<div class="col-12">
				<h2 class="sub-title presentation-title"><?php the_title(); ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-5 col-12">
				<img src="<?php the_post_thumbnail_url(); ?>" alt="thumbnail" class="presentation-thumbnail">
			</div>
			<div class="col-xl-7 col-12">
				<div class="presentation-files">
					<h2 class="sub-title pdf-presentations-title">Available languages</h2>
					<div>
						<?php
							if( have_rows('pdf_repeater') ):
							while ( have_rows('pdf_repeater') ) : the_row();
								?> 
										<a href="<?php the_sub_field('pdf_file'); ?>" class="button_gray"><?php the_sub_field('language'); ?></a>							   
								<?php					   
							endwhile;					   
						endif;
						?>
					</div>
				</div>
				<div class="presentation-info">
					<p><span>Type: </span><?php the_field('type'); ?></p>
					<p><span>Upload date: </span><?php the_field('upload_date'); ?></p>
				</div>
			</div>
		</div>
		
	<?php endwhile; ?>
	<?php endif; ?>
	</div>


	<div class="ib-link-builder col-xl-4">
		<iframe src="https://widgets-m.fxpro.com/en/statistics/ib-link" style="width: 100%; height: 400px; border: none;"></iframe>
	</div>
</div>