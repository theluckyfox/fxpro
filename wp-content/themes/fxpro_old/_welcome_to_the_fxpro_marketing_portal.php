<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-xl-12 col-lg-12">
            <h2 class='marketing-title'><?php pll_e('Welcome to the FxPro Marketing Portal')?></h2>
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-xl-12 col-lg-12">
            <p><?php pll_e('Our marketing portal has been created to provide you with all 
            the resources you need to create an attractive, informative and 
            efficient website. We believe that helping you provide more value 
            for your clients translates to a higher rate of conversion as well 
            as more commissions for you.')?></p>   
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-xl-12 col-lg-12">
            <h2 class='marketing-title'><?php pll_e('FxPro Live Widgets')?> <span><?php pll_e('See all widgets')?></span> </h2>
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-xl-12 col-lg-12 carouselWelcomPage">
        <!-- Slider -->
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="col-6">
                            <h6><?php pll_e('cTrader Live Market Depth Widget')?></h6>
                            <p><?php pll_e('Provide your clients with advanced market data normally reserved for industry professionals.')?></p>
                            <ul>
                                <li>- <?php pll_e('Live market depth information straight to your website.')?></li>
                                <li>- <?php pll_e('Live rates from FxPro`s liquidity providers.')?></li>
                                <li>- <?php pll_e('Data on a range of currency pairs.')?></li>
                            </ul> 
                            <p><a href=""><?php pll_e('Get FxPro cTrader Live Market Depth Widget')?></a></p>  
                        </div>
                        <div class="col-4">
                            <img src="<?php echo(get_template_directory_uri().'/assets/img/page/widget-2.png') ?>" alt="">
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                <div class="row">
                    <div class="col-6">
                        <h6><?php pll_e('FxPro Webinars Widget')?></h6>
                        <p><?php pll_e('Expand the content you make available to your clients with FxPro`s webinar widget.')?></p>
                        <ul>
                            <li>- <?php pll_e('Allow users to register from your page.')?></li>
                            <li>- <?php pll_e('Live rates from FxPro`s liquidity providers.')?></li>
                            <li>- <?php pll_e('Expand the educational resources you have available.')?></li>
                        </ul> 
                        <p><a href=""><?php pll_e('Get FxPro Webinars Widget')?></a></p>  
                    </div>
                    <div class="col-4">
                        <img src="<?php echo(get_template_directory_uri().'/assets/img/page/widget-3.png') ?>" alt="">
                    </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            </div>
        <!-- /Slider -->
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <h5 class='marketing-title'><?php pll_e('Logos')?> <a class="see" href=""><?php pll_e('See all links')?></a></h5>
            <p><?php pll_e('Get the right look with a wide selection of logos, in vector and 
            jpg, to suit the specific needs of your website.')?></p>
        </div>
        <div class="col-4">
            <h5 class='marketing-title'><?php pll_e('Screenshots')?> <a class="see" href=""><?php pll_e('See all links')?></a></h5>
            <p><?php pll_e('For more technically oriented content we provide you with a variety 
            of high quality screenshots of all of our platforms, so your clients are 
            always in the picture.')?></p>
        </div>
        <div class="col-4">
            <h5 class='marketing-title'><?php pll_e('Videos')?> <a class="see" href=""><?php pll_e('See all links')?></a></h5>
            <p><?php pll_e('Break up your webpage with useful audio-visual content to quickly 
            convey information and keep users on your site for longer.')?></p> 
        </div>
        <div class="col-4">
            <h5 class='marketing-title'><?php pll_e('Banners')?> <a class="see" href=""><?php pll_e('See all links')?></a></h5>
            <p><?php pll_e('Browse our comprehensive catalogue of banners to find advertising 
            tailored specifically to your market. Simply input your IB ref and add 
            to your page.')?></p> 
        </div>
        <div class="col-4">
            <h5 class='marketing-title'><?php pll_e('Launch Links')?> <a class="see" href=""> <?php pll_e('See all links')?></a> </h5>
            <p><?php pll_e('Embed launch links to allow your clients to download our platforms 
            directly from your site, or to launch web-based versions of our platforms.')?></p>
        </div>
    </div>
</div>