<section class="footer" style="margin-top: 100px;">
    <div class="container-fluid">
        <div class="row no-gutters copyright align-items-center">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-7">
                <ul class="doc-links">
                    <li class="align-items-center"><a href="https://www.fxpro.co.uk/documents/legal-information">Legal Documentation</a></li>
                    <li class="align-items-center"><a href="https://www.fxpro.co.uk/documents/privacy-policy">Privacy Policy</a></li>
                    <li class="align-items-center"><a href="https://www.fxpro.co.uk/documents/risk-disclosure">Risk Disclosure</a></li>
                </ul>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-5">
                <div class="row social ">
                    <div class="col-2 col-xl-1 offset-xl-6 text-right"><a href=""><i class="fab fa-twitter"></i></a></div>
                    <div class="col-2 col-xl-1 text-right"><a href=""><i class="fab fa-facebook-f"></i></a></div>
                    <div class="col-2 col-xl-1 text-right"><a href=""><i class="fab fa-youtube"></i></a></div>
                    <div class="col-2 col-xl-1 text-right"><a href=""><i class="fab fa-linkedin"></i></a></div>
                    <div class="col-2 col-xl-1 text-right"><a href=""><i class="fab fa-google-plus-g"></i></a></div>
                </div>
            </div>
        </div>
        <div class="row info text-justify">
            <div class="col">
                <p><i class="fas fa-info-circle" style="color: green"></i>
                    <a href="" class="a">Trade Responsibly:</a> Contracts for Difference (‘CFDs’) are complex financial products that are traded on margin. 
                    Trading CFDs carries a high level of risk since leverage can work both to your advantage and disadvantage. As
                        a result, CFDs may not be suitable for all investors because you may lose all your invested capital. You 
                        should not risk more than you are prepared to lose. Before deciding to trade, you need to ensure that you 
                        understand the risks involved taking into account your investment objectives and level of experience. Past 
                        performance of CFDs is not a reliable indicator of future results. Most CFDs have no set maturity date. Hence, 
                        a CFD position matures on the date you choose to close an existing open position. Seek independent advice, 
                        if necessary. Please read FxPro’s full <a href="" class="a">‘Risk Disclosure Notice’</a> .
                </p>
            </div>
        </div>
        
        <div class="row info_2_1">
            <div class="col-12">
                <h3>Truly global</h3>
            </div>
        </div>
        <div class="row ">      
            <!-- 1 -->
            <div class="col-1 uk flag"></div>   
            <div class="col-4 text-justify flag-content">
                <p>FxPro UK Limited is authorised and regulated by the Financial 
                    Conduct Authority <a href="" class="a">(registration no. 509956)</a>.</p>
            </div>   
            
            <div class="col-1 dubai flag"></div>   
            <div class="col-5 text-justify flag-content">
                <p>FxPro Global Markets MENA Limited is authorised and regulated 
                    by the Dubai Financial Services Authority  <a href="" class="a">(reference no. F003333)</a>.</p>
            </div>
        </div>      
        <div class="row info_2_2">
                <!-- 2 -->
            <div class="col-1 eu flag"></div>   
            <div class="col-4 text-justify flag-content">
                <p>FxPro Financial Services Limited is authorised and regulated by the Cyprus Securities and Exchange 
                    Commission <a href="" class="a">(licence no. 078/07)</a>  and authorised by the Financial Services 
                    Board ('FSB')  <a href="" class="a">(authorisation no. 45052)</a>.</p>
            </div>   
            <div class="col-1 bs flag"></div>   
            <div class="col-5 text-justify flag-content">
                <p>FxPro Global Markets Ltd is authorised and regulated by the SCB  <a href="" class="a">(license no. SIA-F184)</a>.</p>
            </div>   
        </div>
        
        <div class="row info_3">
            <div class="col-12 text-justify">
                <p><strong>FxPro Group Limited</strong>  is the holding company of FxPro Financial Services Ltd, FxPro UK Limited, 
                    FxPro Global Markets MENA Limited and FxPro Global Markets Limited.</p>   
            </div>
            <div class="col-12 text-justify">
                <p><strong>FxPro Financial Services Ltd, FxPro UK Limited, FxPro Global Markets MENA Limited, FxPro Global Markets Limited </strong>
                    do not offer Contracts for Difference to residents of certain jurisdictions such as the United States of America,
                        the Islamic Republic of Iran and Canada. With regards to the FSB authorisation, FxPro provides execution services
                        and enters into principal to principal transactions with its clients on FxPro's prices; these transactions are
                        not traded on an exchange. In addition, Contract for Differences (CFDs) with FxPro are not regulated by the FAIS 
                        Act and intermediary services are not provided.</p>
            </div>
        </div>
    </div>
</section>