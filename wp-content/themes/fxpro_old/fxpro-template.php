<?php
    /**
     * Template Name: FxPro
     */
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>
		<?php echo wp_get_document_title(); ?>
    </title>
    <?php wp_head(); ?>
  </head>
  <body>
    <!-- Header  -->
    <?php get_header(); ?>
    <!-- /Header  -->
    
    <!-- Content  -->
    <section class="content">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-md-4 col-lg-4 col-xl-3 d-none d-sm-none d-md-block d-lg-block d-xl-block fxpro_menu">
                    <!-- Menu -->
                    <?php wp_nav_menu( [
                        'menu'              => 'fxproMenu',
                        'container'         => 'div',
                        'container_class'   => 'myMenu',
                        'menu_class'        => 'myMenu',
                        'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    
                    ] ); ?>
                </div>
                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 dynamic-content">
                    <div class="container-fluid">
                    <!-- Content-dynamic  -->
                    <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                        <?php the_content(); ?>
                        <?php endwhile; ?>
	                <?php endif; ?>
                    <?php
                    // if ( have_posts() ){
                    //     while ( have_posts() ){
                    //         the_post();
                
                    //         echo '<h3><a href="'. get_permalink() .'">'. get_the_title() .'</a></h3>';
                
                    //         echo get_the_excerpt(); 
                    //     }
                    // }

                    
                   
                    
                    if (is_page('requests')){
                        get_template_part('src/template/requests');
                    } 
                    if (is_page('launch-links')){
                        get_template_part('src/template/launch_links');
                    }
                    if (is_page('logos')){
                        get_template_part('src/template/logos');
                    }

                    ?>
                    <!-- /Content-dynamic  -->

                    </div>
                    
                    <!-- Footer  -->
                    <?php get_footer(); ?>
                    <!-- /Footer  -->
                </div>
            </div>
        </div>
    </section>
    <!-- /Content  -->
    
    
    
    <?php wp_footer(); ?>
    
      
    </body>
</html>