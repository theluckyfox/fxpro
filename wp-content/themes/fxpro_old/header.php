<!-- <div class="container-fluid header"> -->
    <div class="row align-items-center no-gutters header">
    <!-- 1 header block -->
        <div class="col-auto col-sm-auto col-md-4 col-lg-3 col-xl-3 logo d-flex align-items-center align-self-center">
            <a href="<?= home_url(); ?>">
                <p class="title"><i class="icon-marketing"></i>FxPro Marketing Portal</p>
            </a>
        </div>
    <!-- 2 header block -->
        <div class="col-9 col-sm-6 col-md-8 col-lg-9 col-xl-9">
            <div class="row no-gutters align-items-center justify-content-between">
    <!-- Search -->
                <div class="col-4">
                    <div class="search search-block col-auto">
                        <input type="text" class='form-control'>
                    </div>
                </div>
    <!-- Buttons -->
                <div class="col-auto buttons-group">
                    <!-- <div class="buttons-group"> -->
                        <a href="<?= home_url(); ?>" class="btn-home">FxPro Home Page</a>
                        <a href="https://direct.fxpro.com/user/login?lang=en" class="btn-log-in ">Log in</a>
                        <a href="https://direct.fxpro.com/register?lang=en#step1" class="btn-create-account">Create Account</a>
                    <!-- </div> -->
                </div>
    <!-- PollyLang Language icon -->
                <div class="col-1 col-md-2 col-lg-1 language align-self-center">
                    <div class="lang-flag ">
                        <img src="<?= get_template_directory_uri().'/assets/img/flag/pollyUk.png' ?>" alt="" style="width: 30px; height: 22px;">
                    </div>
                    <!-- <ul>
                        <?php 
                            // pll_the_languages([
                            //     "show_names" => 0,
                            //     "show_flags" => 1
                            // ]); 
                        ?>
                    </ul> -->

                    
                </div>
                <div class="lang-wrapper">
                    <p>RU</p>
                    <p>DE</p>
                    <p class="language-close">Close</p>
                </div>
            </div>
        </div>
    </div>
<!-- </div> -->

<!-- <div class="container-fluid">
    <div class="row d-flex align-content-center flex-wrap">
        <div class="col-3">
            <div class="icon-marketing"></div>
        </div>
        <div class="col-4">2</div>
        <div class="col-4">3</div>
        <div class="col-1">4</div>
    </div>
</div> -->