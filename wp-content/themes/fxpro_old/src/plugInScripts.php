<?php
    add_action( 'wp_enqueue_scripts', function(){
        // wp_enqueue_script('jq', 'https://code.jquery.com/jquery-3.2.1.slim.min.js', array('jquery') );
        wp_enqueue_script('jquery');
        wp_enqueue_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array('jquery') );
        wp_enqueue_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array('jquery') );
        wp_enqueue_script('menu', get_template_directory_uri() .'/assets/js/menu.js', array('jquery') );
        
    });

?>